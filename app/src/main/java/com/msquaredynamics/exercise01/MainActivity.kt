package com.msquaredynamics.exercise01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.button).setOnClickListener {
            Toast.makeText(this, R.string.main_button_click_message, Toast.LENGTH_SHORT).show()
        }

        val imageView = findViewById<ImageView>(R.id.imageView)

        /**
         * Glide is a library that helps loading, displaying and managing pictures.
         * In this case, loading the english-language country_image resource directly
         * into the imageView can cause an error in some devices since the image is big.
         * Glide automatically resizes the picture to decrease its memory usage.
         * It is not mandatory to use it of course
         */
        val options = RequestOptions().apply {
            /* Glides by default caches resources. Providing a signature equals to the language iso3 code,
             * makes Glide to refresh the content of the imageView when the system language changes.
             */
            signature(ObjectKey(Locale.getDefault().isO3Language))
        }
        Glide.with(this).load(R.drawable.country_image)
            .apply(options)
            .into(imageView)

    }
}
